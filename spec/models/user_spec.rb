require 'rails_helper'


RSpec.describe User, type: :model do

  context 'New User' do
  
  context 'Validation of attribute presence' do
    
    it 'ensures password presence' do
      user = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',email:"kengdudee@gmail.com").save
      expect(user).to eq(false)
    end
    it 'ensures email presence' do
      user = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',password:'123456').save
      expect(user).to eq(false)
    end
    it 'ensures first name presence' do
      user = User.new(last_name:'Samatayamontri',password:'123456',email:"kengdudee@gmail.com").save
      expect(user).to eq(false)
    end
    it 'ensures last name presence' do
      user = User.new(first_name:'Supakitpon',password:'123456',email:"kengdudee@gmail.com").save
      expect(user).to eq(false)
    end
    
    it 'should save successfully' do
      user = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',password:'123456',email:"kengdudee@gmail.com").save
      expect(user).to eq(true)
    end
  end

  context 'Validation of email format' do
    it 'will be good format' do
      user = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',password:'123456',email:"kengdudeegmail.com").save
      expect(user).to eq(false)
    end 
  end

  context 'Validation of email uniqueness' do
    it 'will be uniqueness' do
      user1 = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',password:'123456',email:"kengdudeegmail.com").save
      user2 = User.new(first_name:'Supittchar',last_name:'Pootipinyowat',password:'diff6666',email:"kengdudeegmail.com").save
      expect(user2).to eq(false)
    end 
  end

  context 'Validation of length' do
    it 'will longer than 3 character for first name and last name' do
      user = User.new(first_name:'Su',last_name:'Sa',password:'123456',email:"kengdudeegmail.com").save
      expect(user).to eq(false)
    end 
    it 'will longer than 6 character for password' do
      user = User.new(first_name:'Supakitpon',last_name:'Samatayamontri',password:'12345',email:"kengdudee@gmail.com").save
      expect(user).to eq(false)
    end 
  end

end





  
  
end
