require "rails_helper"

RSpec.describe TripsController, type: :routing do

    describe "Routing" do
        it "routes to #checkout" do
            expect(:get => "/checkout").to route_to("trips#checkout")
        end
    end

end