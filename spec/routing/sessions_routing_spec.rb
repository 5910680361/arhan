require "rails_helper"

RSpec.describe SessionsController, type: :routing do

    describe "Routing" do
        it "routes to #signin" do
            expect(:get => "/signin").to route_to("sessions#signin")
        end
        it "routes to #create" do
            expect(:post => "/signin").to route_to("sessions#create")
        end
        it "routes to #destroy" do
            expect(:post => "/logout").to route_to("sessions#destroy")
        end
        it "routes to #failure" do
            expect(:get => "/auth/failure").to route_to("sessions#failure")
        end
    end
    

end