require "rails_helper"

RSpec.describe OrdersController, type: :routing do

    describe "Routing" do
        it "routes to #add_meal" do
            expect(:get => "/add_meal").to route_to("orders#add_meal")
        end
    end

end