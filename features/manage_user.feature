Feature: Manage users
  
  
    Scenario: Signup website
        Given I am on the signup page
        When I fill in "user_first_name" with "tt"
        And I fill in "user_last_name" with "tt"
        And I fill in "user_email" with "t@gmail.com"
        And I fill in "user_password" with "1234"
        And I fill in "user_password_confirmation" with "1234"
        And I press "Create User"
        Then I go to the home page

    Scenario: Signin website
        Given I am on the signin page
        When I fill in "user_email" with "t@gmail.com"
        And I fill in "user_password" with "1234"
        And I press "Sign In"
        Then I go to the home page

    Scenario:Find menu
        Given I am on the home page
        When I click "menu"
        Then I go to the menu page
        And I should see wording "Meal"
        And I should see wording "Price"

    Scenario:Visit about page
        Given I am on the home page
        When I click "about"
        Then I go to the about page
        And I should see wording "User Stories"

    Scenario:Filter menu
        Given I am on the menu page
        When I select "Main" from "category"
        And I select "Low Cal" from "calorie_count"
        And I press "Filter"
        

   

       
        
