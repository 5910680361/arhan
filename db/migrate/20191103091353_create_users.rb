class CreateUsers < ActiveRecord::Base
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.string :f_name
      t.string :l_name
      t.string :address
      t.string :tele

      t.timestamps
    end
  end
end
